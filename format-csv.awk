# Wakatime's --extra-heartbeats flag invokes readline()->json.decode()
# each line is decoded independently, and should be an array of
# wakatime heartbeat messages

BEGIN {
    seen=0
    printf "["
}
END {
    printf "]"
}
{
    # TODO(tamsky): batch JSON lines into groups of N messages

    current_ts=$1 ;
    oFS=FS ; FS="/" ; $0 = $2 ; FS=oFS ;
    # when url is the same
    if ( $3 == last_entity ) {
        # de-duplicate timestamps within 60s
        if ( (last_ts - current_ts) < 60 ) {
                next ;
            }
    }
    # if not the first record, print a comma
    if (seen == 1) {
        printf ","
    }
    # trim common prefixes from entity
    sub("^(www|wiki|forum|cdn)\\.","",$3)
    # output JSON
    printf "{ \"entity\": \"" $3 "\", " \
           "\"entity_type\": \"domain\", " \
           "\"timestamp\": " current_ts ", " \
           "\"project\": \"WEB-" PROFILE_SLUG "\" " \
           "}" ;
    last_ts=current_ts ;
    last_entity=$3 ;
    seen=1 ;
}
