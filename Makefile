# example use:
# make tail PROFILE="Profile 12" START_GATHER_UNIXDATE=$(gdate --date="last tuesday" +%s)


PROFILE ?= Default
PROFILE_PATH ?= $(HOME)/Library/Application Support/Google/Chrome/$(PROFILE)
PROFILE_SLUG ?= $(shell jq -r .profile.name < "$(PROFILE_PATH)/Preferences")

ifdef SELECT_LIMIT
SELECT_LIMIT_EXPR=LIMIT $(SELECT_LIMIT)
else
SELECT_LIMIT_EXPR=
endif

LAST_GATHER_FILE := .last-heartbeat-chrome-profile-$(PROFILE_SLUG)
START_GATHER_UNIXDATE ?= $(shell cat $(LAST_GATHER_FILE))
END_GATHER_UNIXDATE ?= $(shell echo $$(( $(START_GATHER_UNIXDATE) + 86400 )) )

#SELECT last_visit_time, datetime( (visit_time  / 1000000) + (strftime('%s', '1601-01-01')), 'unixepoch') \

SINCE_LAST_GATHER_JOINED = \
SELECT strftime('%s', datetime( (visit_time  / 1000000) + (strftime('%s', '1601-01-01')), 'unixepoch')) \
AS     visit_t,         urls.url  \
FROM visits \
INNER JOIN urls \
ON visits.url = urls.id \
WHERE visit_t >= '$(START_GATHER_UNIXDATE)' AND \
      visit_t < '$(END_GATHER_UNIXDATE)' \
ORDER BY visit_t ASC \
$(SELECT_LIMIT_EXPR) \
; \

#WHERE visit_t >= datetime('$(LAST_GATHER_DATESTAMP)','-1 second') \

# https://wakatime.com/developers/#heartbeats
# POST users/current/heartbeats
# {
#   "entity": <string: entity heartbeat is logging time against, such as an absolute file path or domain>,
#   "type": <string: type of entity; can be file, app, or domain>,
#   "category": <string: category for this activity; can be coding, building, indexing, debugging, browsing, running tests, writing tests, manual testing, code reviewing, or designing>,
#   "time": <float: UNIX epoch timestamp; numbers after decimal point are fractions of a second>,
#   "project": <string: project name (optional)>,
#   "branch": <string: branch name (optional)>,
#   "language": <string: language name (optional)>,
#   "dependencies": <string: comma separated list of dependencies detected from entity file (optional)>,
#   "lines": <integer: total number of lines in the entity (when entity type is file)>,
#   "lineno": <integer: current line row number of cursor (optional)>,
#   "cursorpos": <integer: current cursor column position (optional)>,
#   "is_write": <boolean: whether this heartbeat was triggered from writing to a file (optional)>,
# }



all: copy tail

copy:
	@echo copying history from $(PROFILE_PATH)
	@echo Profile name: $(PROFILE_SLUG)
	@echo -n account email: $(shell jq -r .account_info[0].email < "$(PROFILE_PATH)/Preferences")
	@echo
	cp -v "$(PROFILE_PATH)/History" /tmp/$(PROFILE_SLUG)_History

tail:
	@sqlite3 -batch -csv -cmd "$(SINCE_LAST_GATHER_JOINED)" /tmp/$(PROFILE_SLUG)_History </dev/null | \
            awk -v PROFILE_SLUG=$(PROFILE_SLUG) -F, -f format-csv.awk \
                > /tmp/$(PROFILE_SLUG)_History.heartbeats.json ; \
	LINES_OUT=$$(sed "s/},{/},\\"$$'\n'"{/g" < /tmp/$(PROFILE_SLUG)_History.heartbeats.json|wc -l) ; \
	echo FOUND $$LINES_OUT heartbeats


test:
	sqlite3 -batch -line -cmd "$(SINCE_LAST_GATHER_JOINED)" /tmp/History </dev/null

send_tail:
	touch /tmp/WEB

	wakatime \
                 --verbose \
                 --file /tmp/WEB \
                 --entity domain \
                 --local-file /tmp/WEB \
                 --extra-heartbeats \
                 --project WEB-$(PROFILE_SLUG) \
                 < /tmp/$(PROFILE_SLUG)_History.heartbeats.json
	mv -v /tmp/$(PROFILE_SLUG)_History.heartbeats.json /tmp/$(PROFILE_SLUG)_History.heartbeats.json.sent
